#include <stdio.h>
#include <stdlib.h>
#include <cstdio>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <iomanip>
#include <queue>
#include <vector>
#include <windows.h>
#include <glut.h>
#include "bitmap_image.hpp"
using namespace std;

#define pi (2*acos(0.0))
#define FOV 90
#define EPSILON 0.000001

double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;
int recurLevel = 0;
int pixel = 0;
int noOfObjects = 0;
int noOfLights = 0;
int pyramidsDeclared = 0;
int spheresDeclared = 0;
bool isTexture = true;
GLuint texture;
class Vector;
class Point
{
public:
	double x,y,z,w;
	Point()
	{
	    x = 0;
	    y = 0;
	    z = 0;
        w = 1;
	}
	double operator,(const Vector& v);
	double operator,(const Point& p)
	{
	    return ((this->x * p.x)+(this->y * p.y)+(this->z * p.z));
	}
    Point operator*(double s)
	{
		Point retV;
		retV.x = this->x * s;
		retV.y = this->y * s;
		retV.z = this->z * s;
		retV.w = this->w;
		return retV;
	}
	Point(double x, double y, double z, double w)
	{
	    this->x = x/w;
	    this->y = y/w;
	    this->z = z/w;
	    this->w = 1;
	}
	Point& operator=(const Point& p)
	{
	    this->x = p.x;
	    this->y = p.y;
	    this->z = p.z;
	    this->w = p.w;
	    return *this;
	}
    Point operator+(const Vector& v);
	Vector operator-(const Point& p);
}pos;

class Vector
{
public:
	double x, y, z;
	Vector()
	{
	    x = 0;
	    y = 0;
	    z = 0;
	}
	Vector(double x, double y, double z)
	{
	    this->x = x;
	    this->y = y;
	    this->z = z;
	}
	Vector operator+(const Vector& v)
	{
		Vector retV;
		retV.x = this->x + v.x;
		retV.y = this->y + v.y;
		retV.z = this->z + v.z;
		return retV;
	}
	Point operator+(const Point& p)
	{
		Point retV;
		retV.x = this->x + p.x/p.w;
		retV.y = this->y + p.y/p.w;
		retV.z = this->z + p.z/p.w;
		retV.w = 1;
		return retV;
	}
	Vector operator-(const Vector& v)
	{
		Vector retV;
		retV.x = this->x - v.x;
		retV.y = this->y - v.y;
		retV.z = this->z - v.z;
		return retV;
	}
	Vector operator*(const Vector& v)
	{
		Vector retValue;
		retValue.x = this->y*v.z - this->z*v.y;
		retValue.y = this->z*v.x - this->x*v.z;
		retValue.z = this->x*v.y - this->y*v.x;
		return retValue;
	}
    double operator,(const Point& p)
	{
	    return ((this->x * p.x)+(this->y * p.y)+(this->z * p.z));
	}
	double operator,(const Vector& v)
	{
		return ((this->x * v.x) + (this->y * v.y) + (this->z * v.z));
	}
	Vector operator*(double s)
	{
		Vector retV;
		retV.x = this->x * s;
		retV.y = this->y * s;
		retV.z = this->z * s;
		return retV;
	}
	Vector& operator=(const Vector& v)
	{
	    this->x = v.x;
	    this->y = v.y;
	    this->z = v.z;
	    return *this;
	}
	void normalize()
	{
	    double magnitude = (this->x * this->x) + (this->y * this->y) + (this->z * this->z);
	    magnitude = sqrt(magnitude);
	    //cout << "magnitude = " << magnitude << endl;
	    this->x = this->x / magnitude;
	    this->y = this->y / magnitude;
	    this->z = this->z / magnitude;
	}
	double magnitude()
	{
        double magnitude = (this->x * this->x) + (this->y * this->y) + (this->z * this->z);
	    magnitude = sqrt(magnitude);
	    return magnitude;
	}
}u, r, l;

Point Point::operator+(const Vector& v)
{
	Point retV;
	retV.x = this->x/this->w + v.x;
	retV.y = this->y/this->w + v.y;
	retV.z = this->z/this->w + v.z;
	retV.w = 1;
	return retV;
}

Vector Point::operator-(const Point& p)
{
	Vector retV;
	retV.x = this->x/this->w - p.x/p.w;
	retV.y = this->y/this->w - p.y/p.w;
	retV.z = this->z/this->w - p.z/p.w;
	return retV;
}

double Point::operator,(const Vector& v)
{
    return ((this->x * v.x)+(this->y * v.y)+(this->z * v.z));
}

class Pyramid
{
public:
    Point lowestP;
    double width;
    double height;
    double color[3];
    double ambientc;
    double diffusec;
    double specularc;
    double reflectionc;
    double shininess;
    Point basePoints[4];
    Point topPoint;
    void print()
    {
        cout << "pyramid" << endl;
        cout << lowestP.x << " " << lowestP.y << " " << lowestP.z << endl;
        cout << width << " " << height << endl;
        cout << color[0] << " " << color[1] << " " << color[2] << endl;
        cout << ambientc << " " << diffusec << " " << specularc << " " << reflectionc << endl;
        cout << shininess << endl;
    }
    void calculatePoints()
    {
        basePoints[0] = lowestP;
        basePoints[1].x = lowestP.x + width;
        basePoints[1].y = lowestP.y;
        basePoints[1].z = lowestP.z;
        basePoints[2].x = lowestP.x + width;
        basePoints[2].y = lowestP.y + width;
        basePoints[2].z = lowestP.z;
        basePoints[3].x = lowestP.x;
        basePoints[3].y = lowestP.y + width;
        basePoints[3].z = lowestP.z;
        topPoint.x = lowestP.x + width/2;
        topPoint.y = lowestP.y + width/2;
        topPoint.z = lowestP.z + height;
    }
}*pyramids;

class Sphere
{
public:
    Point centre;
    double radius;
    double color[3];
    double ambientc;
    double diffusec;
    double specularc;
    double reflectionc;
    double shininess;
    void print()
    {
        cout << "sphere" << endl;
        cout << centre.x << " " << centre.y << " " << centre.z << endl;
        cout << radius << endl;
        cout << color[0] << " " << color[1] << " " << color[2] << endl;
        cout << ambientc << " " << diffusec << " " << specularc << " " << reflectionc << endl;
        cout << shininess << endl;
    }
}*spheres;

class Triangle
{
public:
    Point p1;
    Point p2;
    Point p3;
};

class LightSource
{
public:
    Point pos;
}*lights;

Vector rotate(Vector v, Vector vcbasis, double angle)
{
    Vector retValue;
    retValue.x = v.x*cos(angle) +  vcbasis.x*sin(angle);
    retValue.y = v.y*cos(angle) +  vcbasis.y*sin(angle);
    retValue.z = v.z*cos(angle) +  vcbasis.z*sin(angle);
    return retValue;
}

void up()
{
	u = rotate(u, l, ((double)3/(double)360)*(2*pi));
	l = u*r;
}

void down()
{
	u = rotate(u, l, -((double)3 / (double)360)*(2 * pi));
	l = u*r;
}

void left()
{
	r = rotate(r, l, ((double)3 / (double)360)*(2 * pi));
	l = u*r;
}

void right()
{
	r = rotate(r, l, -((double)3 / (double)360)*(2 * pi));
	l = u*r;
}

void tiltClockWise()
{
	u = rotate(u, r, -((double)3 / (double)360)*(2 * pi));
	r = l*u;
}

void tiltCClockWise()
{
	u = rotate(u, r, ((double)3 / (double)360)*(2 * pi));
	r = l*u;
}


void drawAxes()
{
	if(drawaxes==1)
	{
		glBegin(GL_LINES);{
			glColor3f(1.0, 0,0);
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glColor3f(0, 1.0, 0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glColor3f(0, 0, 1.0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}

void drawSquare2(Point p, double width)
{
    glBegin(GL_QUADS);{
        glVertex3f(p.x, p.y, p.z);
        glVertex3f(p.x + width, p.y, p.z);
        glVertex3f(p.x + width, p.y + width, p.z);
        glVertex3f(p.x, p.y + width, p.z);
    }glEnd();
}

void drawGrid(double initcol, double width)
{
	double col = initcol;
    for(int x = 0; x < 100*width; x+=width)
    {
        col = 1 - col;
        for(int y = 0; y < 100*width; y+=width)
        {
            Point p;
            p.x = x;
            p.y = y;
            p.z = 0;
            col = 1 - col;
            glColor3f(col, col, col);
            drawSquare2(p, width);
        }
    }
}

GLuint loadTexture(string filename);

void drawTextureSquare(Point p, double width, GLuint texture)
{
    glEnable( GL_TEXTURE_2D );
    glBindTexture( GL_TEXTURE_2D, texture );
    glBegin(GL_QUADS);{
        glTexCoord2f(0.0, 0.0);glVertex3f(p.x, p.y, p.z);
        glTexCoord2f(0.0, 1.0);glVertex3f(p.x + width, p.y, p.z);
        glTexCoord2f(1.0, 1.0);glVertex3f(p.x + width, p.y + width, p.z);
        glTexCoord2f(1.0, 0.0);glVertex3f(p.x, p.y + width, p.z);
    }glEnd();
    glDisable( GL_TEXTURE_2D );
}

void drawTexturedGrid(double width)
{
    GLuint texture = loadTexture("texture.bmp");
    for(int x = 0; x < 100*width; x+=width)
    {
        for(int y = 0; y < 100*width; y+=width)
        {
            Point p;
            p.x = x;
            p.y = y;
            p.z = 0;
            drawTextureSquare(p, width, texture);
        }
    }

}

void drawFullGrid(double width)
{
    double initcol = 0;
    glPushMatrix();{
        if(!isTexture) drawGrid(initcol, width);
        else drawTexturedGrid(width);
        initcol = 1 - initcol;
        glRotated(90, 0, 0, 1);
        if(!isTexture) drawGrid(initcol, width);
        else drawTexturedGrid(width);
        initcol = 1 - initcol;
        glRotated(90, 0, 0, 1);
        if(!isTexture) drawGrid(initcol, width);
        else drawTexturedGrid(width);
        initcol = 1 - initcol;
        glRotated(90, 0, 0, 1);
        if(!isTexture) drawGrid(initcol, width);
        else drawTexturedGrid(width);
    }glPopMatrix();
}

void drawSquare(double a)
{
    glColor3f(0.0,0.0,1.0);
	glBegin(GL_QUADS);{
		glVertex3f( a, a,0);
		glVertex3f( a,-a,0);
		glVertex3f(-a,-a,0);
		glVertex3f(-a, a,0);
	}glEnd();
	glColor3d(1, 1, 1);
	glBegin(GL_LINES);
	{
		glVertex3d(a, a, 0);
		glVertex3d(-a, a, 0);
		glVertex3d(a, a, 0);
		glVertex3d(a, -a, 0);
		glVertex3d(-a, a, 0);
		glVertex3d(-a, -a, 0);
		glVertex3d(-a, -a, 0);
		glVertex3d(a, -a, 0);
	}glEnd();
}


void drawCircle(double radius,int segments)
{
    int i;
    struct Point Points[100];
    glColor3f(0.7,0.7,0.7);
    //generate Points
    for(i=0;i<=segments;i++)
    {
        Points[i].x=radius*cos(((double)i/(double)segments)*2*pi);
        Points[i].y=radius*sin(((double)i/(double)segments)*2*pi);
    }
    //draw segments using generated Points
    for(i=0;i<segments;i++)
    {
        glBegin(GL_LINES);
        {
			glVertex3f(Points[i].x,Points[i].y,0);
			glVertex3f(Points[i+1].x,Points[i+1].y,0);
        }
        glEnd();
    }
}

void drawCone(double radius,double height,int segments)
{
    int i;
    double shade;
    struct Point Points[100];
    //generate Points
    for(i=0;i<=segments;i++)
    {
        Points[i].x=radius*cos(((double)i/(double)segments)*2*pi);
        Points[i].y=radius*sin(((double)i/(double)segments)*2*pi);
    }
    //draw triangles using generated Points
    for(i=0;i<segments;i++)
    {
        //create shading effect
        if(i<segments/2)shade=2*(double)i/(double)segments;
        else shade=2*(1.0-(double)i/(double)segments);
        glColor3f(shade,shade,shade);

        glBegin(GL_TRIANGLES);
        {
            glVertex3f(0,0,height);
			glVertex3f(Points[i].x,Points[i].y,0);
			glVertex3f(Points[i+1].x,Points[i+1].y,0);
        }
        glEnd();
    }
}

void drawCylinder(double radius, double height, int slices, int stacks)
{
	struct Point** Points = new Point*[stacks+1];
	for (int i = 0; i <= stacks; i++)
	{
		Points[i] = new Point[slices + 1];
	}
	for (int i = 0; i <= stacks; i++)
	{
		for (int j = 0; j <= slices; j++)
		{
			Points[i][j].x = radius*cos(((double)j / (double)slices)*(2 * pi));
			Points[i][j].y = radius*sin(((double)j / (double)slices)*(2 * pi));
			Points[i][j].z = (((double)i / (double)stacks)*height);
		}
	}
	for (int i = 0; i < stacks; i++)
	{
		glColor3d((double)i / (double)stacks, (double)i / (double)stacks, (double)i / (double)stacks);
		for (int j = 0; j < slices; j++)
		{
			glBegin(GL_QUADS);
			{
				glVertex3d(Points[i][j].x, Points[i][j].y, Points[i][j].z);
				glVertex3d(Points[i][j + 1].x, Points[i][j + 1].y, Points[i][j + 1].z);
				glVertex3d(Points[i + 1][j + 1].x, Points[i + 1][j + 1].y, Points[i + 1][j + 1].z);
				glVertex3d(Points[i + 1][j].x, Points[i + 1][j].y, Points[i + 1][j].z);
			}glEnd();
		}
	}
	for (int i = 0; i <= stacks; i++)
	{
		delete Points[i];
	}
	delete Points;
}

void drawBasicSphere(double radius,int slices,int stacks)
{
	struct Point Points[100][100];
	int i,j;
	double h,r;
	//generate Points
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2));
		r=radius*cos(((double)i/(double)stacks)*(pi/2));
		for(j=0;j<=slices;j++)
		{
			Points[i][j].x=r*cos(((double)j/(double)slices)*(2*pi));
			Points[i][j].y=r*sin(((double)j/(double)slices)*(2*pi));
			Points[i][j].z=h;
		}
	}
	//draw quads using generated Points
	for(i=0;i<stacks;i++)
	{
        //glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
		for(j=0;j<slices;j++)
		{
			glBegin(GL_QUADS);{
			    //upper hemisphere
				glVertex3f(Points[i][j].x,Points[i][j].y,Points[i][j].z);
				glVertex3f(Points[i][j+1].x,Points[i][j+1].y,Points[i][j+1].z);
				glVertex3f(Points[i+1][j+1].x,Points[i+1][j+1].y,Points[i+1][j+1].z);
				glVertex3f(Points[i+1][j].x,Points[i+1][j].y,Points[i+1][j].z);
                //lower hemisphere
                glVertex3f(Points[i][j].x,Points[i][j].y,-Points[i][j].z);
				glVertex3f(Points[i][j+1].x,Points[i][j+1].y,-Points[i][j+1].z);
				glVertex3f(Points[i+1][j+1].x,Points[i+1][j+1].y,-Points[i+1][j+1].z);
				glVertex3f(Points[i+1][j].x,Points[i+1][j].y,-Points[i+1][j].z);
			}glEnd();
		}
	}
}

void drawSphere(Point centre, double radius, double R, double G, double B)
{
    glColor3d(R, G, B);
    glPushMatrix();{
        glTranslated(centre.x, centre.y, centre.z);
        drawBasicSphere(radius, 50, 50);
    }glPopMatrix();

}

void drawSquare(Point p1, Point p2, Point p3, Point p4)
{
    glBegin(GL_QUADS);{
        glVertex3f(p1.x, p1.y, p1.z);
        glVertex3f(p2.x, p2.y, p2.z);
        glVertex3f(p3.x, p3.y, p3.z);
        glVertex3f(p4.x, p4.y, p4.z);
    }glEnd();
}

void drawTriangle(Point p1, Point p2, Point p3)
{
    glBegin(GL_TRIANGLES);{
        glVertex3f(p1.x, p1.y, p1.z);
        glVertex3f(p2.x, p2.y, p2.z);
        glVertex3f(p3.x, p3.y, p3.z);
    }glEnd();
}

void drawPyramid(Point p, double width, double height, double R, double G, double B)
{
    glColor3d(R,G,B);
    glPushMatrix();{
        Point topP;
        topP.x = p.x + width/2;
        topP.y = p.y + width/2;
        topP.z = p.z + height;
        Point sp1;
        Point sp2;
        Point sp3;
        Point sp4;
        sp1 = p;
        sp2.x = p.x + width;
        sp2.y = p.y;
        sp2.z = p.z;
        sp3.x = p.x + width;
        sp3.y = p.y + width;
        sp3.z = p.z;
        sp4.x = p.x;
        sp4.y = p.y + width;
        sp4.z = p.z;
        drawSquare(sp1, sp2, sp3, sp4);
        drawTriangle(sp1, sp2, topP);
        drawTriangle(sp2, sp3, topP);
        drawTriangle(sp3, sp4, topP);
        drawTriangle(sp4, sp1, topP);
    }glPopMatrix();
}

void drawLight(Point p)
{
    drawSphere(p, 0.6, 1, 1, 1);
}

void saveImage();
void keyboardListener(unsigned char key, int x,int y){
	switch(key){
    case '0':
        cout << "saving image....." << endl;
        saveImage();
        break;
    case '1':
        right();
        break;
    case '2':
        left();
        break;
    case '3':
        up();
        break;
    case '4':
        down();
        break;
    case '5':
        tiltClockWise();
        break;
    case '6':
        tiltCClockWise();
        break;
    default:
        break;
	}
}


void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			pos.x -= 3*l.x;
			pos.y -= 3*l.y;
			pos.z -= 3*l.z;
			break;
		case GLUT_KEY_UP:		// up arrow key
			pos.x += 3*l.x;
			pos.y += 3*l.y;
			pos.z += 3*l.z;
			break;

		case GLUT_KEY_RIGHT:
			pos.x += 3*r.x;
			pos.y += 3*r.y;
			pos.z += 3*r.z;
			break;
		case GLUT_KEY_LEFT:
			pos.x -= 3*r.x;
			pos.y -= 3*r.y;
			pos.z -= 3*r.z;
			break;

		case GLUT_KEY_PAGE_UP:
			pos.x += 3*u.x;
			pos.y += 3*u.y;
			pos.z += 3*u.z;
			break;
		case GLUT_KEY_PAGE_DOWN:
			pos.x -= 3*u.x;
			pos.y -= 3*u.y;
			pos.z -= 3*u.z;
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
//			if (assign1length > 0)
//			{
//				assign1length--;
//				assign1radius = 40 - assign1length;
//			}
			break;
		case GLUT_KEY_END:
//			if (assign1radius > 0)
//			{
//				assign1radius--;
//				assign1length = 40 - assign1radius;
//			}
			break;

		default:
			break;
	}
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}

vector<string> split(string str, char delimiter) {
  vector<string> retV;
  stringstream ss(str);
  string tok;
  while(getline(ss, tok, delimiter)) {
    retV.push_back(tok);
  }
  return retV;
}


double GridWidth = 20;

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	//gluLookAt(200*cos(cameraAngle), 200*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,0,200,	0,0,0,	0,1,0);
	gluLookAt(pos.x, pos.y, pos.z, pos.x + l.x, pos.y + l.y, pos.z + l.z, u.x, u.y, u.z);

	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects

	//drawAxes();
	//drawGrid(20);
	//drawAssign1(assign1length, assign1radius, 24, 20);
//	Point p;
//	p.x = 1.0;
//	p.y = 1.0;
//	p.z = 10.0;
//	drawPyramid(p, 20, 50, 1, 0, 0);
    for(int i = 0; i<pyramidsDeclared; i++)
    {
        drawPyramid(pyramids[i].lowestP, pyramids[i].width, pyramids[i].height, pyramids[i].color[0], pyramids[i].color[1], pyramids[i].color[2]);
    }
    for(int i = 0; i<spheresDeclared; i++)
    {
        drawSphere(spheres[i].centre, spheres[i].radius, spheres[i].color[0], spheres[i].color[1], spheres[i].color[2]);
    }
    for(int i = 0; i<noOfLights; i++)
    {
        drawLight(lights[i].pos);
    }
    drawFullGrid(GridWidth);
	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


void animate(){
	//angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//codes for initialization
	drawgrid=0;
	drawaxes=1;
	cameraHeight=150.0;
	cameraAngle=1.0;
	angle=0;

	//initializing the pos and u,r,l Vectors for camera movement
	u.x = 0;
	u.y = 0;
	u.z = 1;
	u.normalize();
	r.x = -((double)1 / sqrt(2));
	r.y = ((double)1 / sqrt(2));
	r.z = 0;
	r.normalize();
	l.x = -((double)1 / sqrt(2));
	l.y = -((double)1 / sqrt(2));
	l.z = 0;
	l.normalize();
	pos.x = (double)100;
	pos.y = (double)100;
	pos.z = 50;



	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(FOV,	1,	1,	1000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

//raytracing codes
class tBPNO
{
public:
    bool isIntersect;
    double t;
    Point p;
    Vector n;
    double color[3];
    double ambientc;
    double diffusec;
    double specularc;
    double reflectionc;
    double shininess;
    double intensity;
    bool operator<(const tBPNO& o) const
    {
        return this->t < o.t;
    }
    bool operator>(const tBPNO& o) const
    {
        return this->t > o.t;
    }
    bool operator==(const tBPNO& o) const
    {
        return this->t == o.t;
    }
    bool operator>=(const tBPNO& o) const
    {
        return this->t >= o.t;
    }
    bool operator<=(const tBPNO& o) const
    {
        return this->t <= o.t;
    }
    tBPNO& operator=(const tBPNO& o)
    {
        this->isIntersect = o.isIntersect;
        this->p = o.p;
        this->t = o.t;
        this->n = o.n;
        this->color[0] = o.color[0];
        this->color[1] = o.color[1];
        this->color[2] = o.color[2];
        this->ambientc = o.ambientc;
        this->diffusec = o.diffusec;
        this->intensity = o.intensity;
        this->reflectionc = o.reflectionc;
        this->shininess = o.shininess;
        this->specularc = o.specularc;
        return *this;
    }
};

tBPNO quadEq(double a, double b, double c)
{
    tBPNO retV;
    retV.isIntersect = false;
    double discriminant = (b*b - 4*a*c);
    if(discriminant < 0)
    {
        return retV;
    }
    double results[2];
    results[0] = ((-b) + sqrt(discriminant))/(2*a);
    results[1] = ((-b) - sqrt(discriminant))/(2*a);
    retV.t = min(results[0], results[1]);
    if(retV.t > EPSILON) retV.isIntersect = true;
    return retV;
}

tBPNO raySphereIntersect(Point ro, Vector rd, Sphere s)
{
    double a = 1;
    double b = ((rd,ro) * 2) - ((rd,s.centre) * 2);
    double c = (ro,ro) - ((s.centre,ro) * 2) + (s.centre,s.centre) - (s.radius * s.radius);
    tBPNO retV = quadEq(a,b,c);
    retV.intensity = 1.0;
    retV.color[0] = s.color[0];
    retV.color[1] = s.color[1];
    retV.color[2] = s.color[2];
    retV.ambientc = s.ambientc;
    retV.diffusec = s.diffusec;
    retV.specularc = s.specularc;
    retV.reflectionc = s.reflectionc;
    retV.shininess = s.shininess;
    retV.p = ro + (rd * retV.t);
    retV.n = retV.p - s.centre;
    retV.n.normalize();
//    cout <<"sphere intersect " << retV.p.x << " " << retV.p.y << " " << retV.p.z << endl;
    //cout << "sphere color : " << retV.color[0] << " " << retV.color[1] << " " << retV.color[2] << endl;
    return retV;
}

tBPNO rayTriangleIntersect(Point ro, Vector rd, Triangle tr)
{
    tBPNO retV;
    Vector t12, t13, P, Q, T;
    double det, invDet, u, v, t;
    t12 = tr.p2 - tr.p1;
    t13 = tr.p3 - tr.p1;
    P = rd * t13;
    det = (t12, P);
    if(det > -EPSILON && det < EPSILON )
    {
        retV.isIntersect = false;
        return retV;
    }
    invDet = 1.0/det;
    T = ro - tr.p1;
    u = (T, P) * invDet;
    if(u < 0.0 || u > 1.0)
    {
        retV.isIntersect = false;
        return retV;
    }
    Q = T * t12;
    v = (rd, Q) * invDet;
    if(v < 0.0 || u + v > 1.0)
    {
        retV.isIntersect = false;
        return retV;
    }
    t = (t13, Q) * invDet;
    if(t > EPSILON) {
        retV.isIntersect = true;
        retV.t = t;
        retV.p = ro + (rd * retV.t);
        retV.n = (t12 * t13);
        retV.n.normalize();
        return retV;
    }
    retV.isIntersect = false;
    return retV;
//    tBPNO retV;
//    Vector v1 = tr.p3 - tr.p1;
//    Vector v2 = tr.p2 - tr.p1;
//    Vector n = v1 * v2;
//    n.normalize();
//    retV.n = n;
//    double denominator = (rd,n);
//    if(denominator > -EPSILON && denominator < EPSILON)
//    {
//        retV.isIntersect = false;
//        return retV;
//    }
//    retV.t = ((tr.p1 - ro),n)/denominator;
//    retV.p = ro + (rd * retV.t);
//    Vector v12 = tr.p2 - tr.p1;
//    Vector v13 = tr.p3 - tr.p1;
//    Vector v1p = retV.p - tr.p1;
//
//    return retV;
}

tBPNO rayPyramidIntersect(Point ro, Vector rd, Pyramid py)
{
    priority_queue<tBPNO, vector<tBPNO>, greater<tBPNO> > intersectList;
    py.calculatePoints();
    Triangle t1,t2,t3,t4,t5,t6;
    t1.p1 = py.basePoints[0];
    t1.p2 = py.basePoints[1];
    t1.p3 = py.topPoint;
    t2.p1 = py.basePoints[1];
    t2.p2 = py.basePoints[2];
    t2.p3 = py.topPoint;
    t3.p1 = py.basePoints[2];
    t3.p2 = py.basePoints[3];
    t3.p3 = py.topPoint;
    t4.p1 = py.basePoints[3];
    t4.p2 = py.basePoints[0];
    t4.p3 = py.topPoint;
    t5.p1 = py.basePoints[2];
    t5.p2 = py.basePoints[1];
    t5.p3 = py.basePoints[0];
    t6.p1 = py.basePoints[2];
    t6.p2 = py.basePoints[0];
    t6.p3 = py.basePoints[3];
    intersectList.push(rayTriangleIntersect(ro, rd, t1));
    intersectList.push(rayTriangleIntersect(ro, rd, t2));
    intersectList.push(rayTriangleIntersect(ro, rd, t3));
    intersectList.push(rayTriangleIntersect(ro, rd, t4));
    intersectList.push(rayTriangleIntersect(ro, rd, t5));
    intersectList.push(rayTriangleIntersect(ro, rd, t6));
    tBPNO retV;
    while(intersectList.top().isIntersect == false)
    {
        intersectList.pop();
        if(intersectList.size() == 0)
        {
            retV.isIntersect = false;
            return retV;
        }
    }
    retV = intersectList.top();
    retV.color[0] = py.color[0];
    retV.color[1] = py.color[1];
    retV.color[2] = py.color[2];
    retV.ambientc = py.ambientc;
    retV.diffusec = py.diffusec;
    retV.specularc = py.specularc;
    retV.reflectionc = py.reflectionc;
    retV.shininess = py.shininess;
    retV.intensity = 1.0;
//    cout << "pyramid intersect: " << retV.p.x << " " << retV.p.y << " " << retV.p.z << endl;
//    cout << "pyramid color : " << retV.color[0] << " " << retV.color[1] << " " << retV.color[2] << endl;
    return retV;
}

class RGB{
public:
   double R;
   double G;
   double B;
   bool isIntersect;
   RGB& operator=(const RGB& rgb)
   {
       this->R = rgb.R;
       this->G = rgb.G;
       this->B = rgb.B;
       return *this;
   }
};

RGB** imageBuffer;
int textureHeight;
int textureWidth;
bool textureNotCopied = true;
unsigned char * imageData;
GLuint loadTexture(string filename)
{
    unsigned char header[54]; // Each BMP file begins by a 54-bytes header
    unsigned int dataStart;     // Position in the file where the actual textureData begins
    unsigned int imageSize;   // = width*height*3
    unsigned char * textureData;
    // Actual RGB data
    FILE * file = fopen(filename.c_str(),"rb");
    if (!file)
    {
        printf("Texture for floor could not be opened\n");
        exit(0);
    }
    if ( fread(header, 1, 54, file)!=54 )  // If not 54 bytes read : problem
    {
        printf("Texture for floor cannot be used, Not a correct BMP file\n");
        exit(0);
    }
    if ( header[0]!='B' || header[1]!='M' )
    {
        printf("Texture for floor cannot be used, Not a correct BMP file\n");
        exit(0);
    }
    dataStart = 54;
    textureWidth = *(int*)&(header[0x12]);
    textureHeight = *(int*)&(header[0x16]);
    imageSize = textureWidth*textureHeight*3;
    textureData = new unsigned char[imageSize];
    fread(textureData,1,imageSize,file);
    fclose(file);
    for(int i = 0; i < textureWidth * textureHeight ; ++i)
    {
        int index = i*3;
        unsigned char B,R;
        B = textureData[index];
        R = textureData[index+2];
        textureData[index] = R;
        textureData[index+2] = B;
    }
    if(textureNotCopied)
    {
        //cout << "declaring image buffer" << endl;
        imageBuffer = new RGB*[textureHeight];
        for(int i = 0; i<textureHeight; i++)
        {
            imageBuffer[i] = new RGB[textureWidth];
        }
        //cout << "image buffer declared" << endl;
        for(int k = 0; k < textureWidth * textureHeight ; ++k)
        {
            int index = k*3;
            unsigned char R,G,B;
            R = textureData[index];
            G = textureData[index+1];
            B = textureData[index+2];
            int i = k / textureWidth;
            int j = k - textureWidth * i;
            imageBuffer[i][j].R = (double)R/255.0;
            imageBuffer[i][j].G = (double)G/255.0;
            imageBuffer[i][j].B = (double)B/255.0;
        }
        textureNotCopied = false;
    }
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, textureWidth, textureHeight, GL_RGB, GL_UNSIGNED_BYTE, textureData );
    return textureID;

}

tBPNO rayTextureIntersect(Point ro, Vector rd)
{
    rd.normalize();
    tBPNO retV;
    retV.n.x = 0;
    retV.n.y = 0;
    retV.n.z = 1;
    Point p;
    p.x = 0;
    p.y = 0;
    p.z = 0;
    double denominator = (rd,retV.n);
    if(denominator > -EPSILON && denominator < EPSILON)
    {
        retV.isIntersect = false;
        return retV;
    }
    retV.t = ((p - ro),retV.n)/denominator;
    if(retV.t < 0)
    {
        retV.isIntersect = false;
        return retV;
    }
    retV.p = ro + (rd * retV.t);
//    int i = (int) (abs((retV.p.y)) / textureHeight);
//    int j = (int) (abs((retV.p.x)) / textureWidth);
//    int imagei = abs((retV.p.y)) - (double)(i*textureHeight);
    double ifrac = fmod((abs(retV.p.y)/(double)GridWidth), 1.0);
    double jfrac = fmod((abs(retV.p.x)/(double)GridWidth), 1.0);
    if(retV.p.x > 0 && retV.p.y < 0 || retV.p.x < 0 && retV.p.y > 0)
    {
        ifrac = 1 - ifrac;
        jfrac = 1 - jfrac;
    }
    int i = (int)(ifrac * ((double)textureHeight));
    int j = (int)(jfrac * ((double)textureWidth));
    retV.color[0] = imageBuffer[i][j].R;
    retV.color[1] = imageBuffer[i][j].G;
    retV.color[2] = imageBuffer[i][j].B;
    retV.ambientc = 0.2;
    retV.diffusec = 0.2;
    retV.specularc = 0.2;
    retV.reflectionc = 0.2;
    retV.shininess = 3;
    retV.intensity = 1;
    retV.isIntersect = true;
    return retV;
}
tBPNO rayGridIntersect(Point ro, Vector rd)
{
    rd.normalize();
    tBPNO retV;
    retV.n.x = 0;
    retV.n.y = 0;
    retV.n.z = 1;
    Point p;
    p.x = 0;
    p.y = 0;
    p.z = 0;
    double denominator = (rd,retV.n);
    if(denominator > -EPSILON && denominator < EPSILON)
    {
        retV.isIntersect = false;
        return retV;
    }
    retV.t = ((p - ro),retV.n)/denominator;
    if(retV.t < 0)
    {
        retV.isIntersect = false;
        return retV;
    }
    retV.p = ro + (rd * retV.t);
    int n = (int) (abs((retV.p.x)) / GridWidth) + (int) (abs((retV.p.y)) / GridWidth);
    int check = 0;
    if((retV.p.x > 0 && retV.p.y > 0) || (retV.p.x < 0 && retV.p.y < 0))
    {
        check = pow(-1, n+1);
    }
    else
    {
        check = pow(-1, n);
    }
    if(check == -1)
    {
        retV.color[0] = 0;
        retV.color[1] = 0;
        retV.color[2] = 0;
    }
    if(check == 1)
    {
        retV.color[0] = 1;
        retV.color[1] = 1;
        retV.color[2] = 1;
    }
    retV.ambientc = 0.2;
    retV.diffusec = 0.2;
    retV.specularc = 0.2;
    retV.reflectionc = 0.2;
    retV.shininess = 3;
    retV.intensity = 1;
    retV.isIntersect = true;
    return retV;
}

class Ray{
public:
    Point ro;
    Vector rd;
    Ray operator=(const Ray& r)
    {
        this->ro = r.ro;
        this->rd = r.rd;
    }
    void print()
    {
        cout << "ro = (" << ro.x << ", " << ro.y << ", " << ro.z << ")" << ", rd = (" << rd.x << ", " << rd.y << ", " << rd.z << ")" << endl;
    }
}**rayList;


void populateRayList()
{
    double t = 1.0/(tan(FOV/2));
    Point centre = pos + (l * t);
    //cout << "centre = " << centre.x << " " << centre.y << " " << centre.z << endl;
    u.normalize();
    r.normalize();
   // cout << "up = " << u.x << " " << u.y << " " << u.z << endl;
    //cout << "right = " << r.x << " " << r.y << " " << r.z << endl;
    Point topLeft = (centre + u) + r;
    Vector left = (r * -1);
    Vector down = (u * -1);
    for(int i = 0; i<pixel; i++)
    {
        double leftmag = (double)(2*i + 1)/(double)pixel;
        for(int j = 0; j<pixel; j++)
        {
            double downmag = (double)(2*j + 1)/(double)pixel;
            Point p = ((topLeft + (left * leftmag)) + (down * downmag));
            rayList[i][j].ro = pos;
            rayList[i][j].rd = (p - pos);
            rayList[i][j].rd.normalize();
            //cout << "rayList point: " << p.x << " " << p.y << " " << p.z << endl;
            //cout << "ray is: " << rayList[i][j].ro.x << " " << rayList[i][j].ro.y << " " << rayList[i][j].ro.z << ", " << rayList[i][j].rd.x << " " << rayList[i][j].rd.y << " " << rayList[i][j].rd.z << endl;
        }
    }
}



Vector reflection(Vector v, Vector normal)
{
    return (v - (normal * ((v,normal) * 2)));
}

bool isShadow(Ray r)
{
    priority_queue<tBPNO, vector<tBPNO>, greater<tBPNO> > intersectList;
    //cout << "error check 1" << endl;
    for(int i = 0; i<spheresDeclared; i++)
    {
        intersectList.push(raySphereIntersect(r.ro, r.rd, spheres[i]));
    }
    //cout << "error check 2"<< endl;
    for(int i = 0; i<pyramidsDeclared; i++)
    {
        intersectList.push(rayPyramidIntersect(r.ro, r.rd, pyramids[i]));
    }
    while(intersectList.top().isIntersect == false)
    {
        intersectList.pop();
        if(intersectList.size() == 0)
        {
            return false;
        }
    }
    return true;
}





RGB computePhong(Ray r, int level)
{
    priority_queue<tBPNO, vector<tBPNO>, greater<tBPNO> > intersectList;
    //cout << "error check 1" << endl;
    for(int i = 0; i<spheresDeclared; i++)
    {
        intersectList.push(raySphereIntersect(r.ro, r.rd, spheres[i]));
    }
//    //cout << "error check 2"<< endl;
    for(int i = 0; i<pyramidsDeclared; i++)
    {
        intersectList.push(rayPyramidIntersect(r.ro, r.rd, pyramids[i]));
    }
    //cout << "error check 3"<< endl;
    if(!isTexture) intersectList.push(rayGridIntersect(r.ro, r.rd));
    else intersectList.push(rayTextureIntersect(r.ro, r.rd));
    while(intersectList.top().isIntersect == false)
    {
        intersectList.pop();
        if(intersectList.size() == 0)
        {
            RGB retV;
            retV.R = 0;
            retV.G = 0;
            retV.B = 0;
            retV.isIntersect = false;
            return retV;
        }
    }
    //cout << "intersection found" << endl;
    tBPNO obj;
    obj.color[0] = intersectList.top().color[0];
    obj.color[1] = intersectList.top().color[1];
    obj.color[2] = intersectList.top().color[2];
    obj.ambientc = intersectList.top().ambientc;
    obj.diffusec = intersectList.top().diffusec;
    obj.intensity = intersectList.top().intensity;
    obj.isIntersect = intersectList.top().isIntersect;
    obj.n = intersectList.top().n;
    //obj.n = obj.n * -1;
    obj.p = intersectList.top().p;
    obj.reflectionc = intersectList.top().reflectionc;
    obj.shininess = intersectList.top().shininess;
    obj.specularc = intersectList.top().specularc;
    obj.t = intersectList.top().t;
   // cout << "actual intersect: " << obj.p.x << " " << obj.p.y << " " << obj.p.z << endl;
    //cout << "actual color: " << obj.color[0] << " " << obj.color[1] << " " << obj.color[2] << endl;
    //cout << "error check 4" << endl;
    RGB col;
    col.isIntersect = true;
    col.R = obj.ambientc * obj.color[0];
    col.G = obj.ambientc * obj.color[1];
    col.B = obj.ambientc * obj.color[2];
    //cout << "after ambient: " << col.R << " " << col.G << " " << col.B << endl;
    //cout << "error check 5" << endl;
    for(int i = 0; i<noOfLights; i++)
    {
        Vector li = lights[i].pos - obj.p;
        li.normalize();
        Ray checkRay;
        checkRay.ro = obj.p + li * 0.001;
        checkRay.rd = li;
        if(isShadow(checkRay)) continue;
        double lambert = (li,obj.n);
        if(lambert < 0) lambert = 0;
        Vector v;
        v.x = -r.rd.x;
        v.y = -r.rd.y;
        v.z = -r.rd.z;
        v.normalize();
        Vector h = v + li;
        h.normalize();
        double phong = (h,obj.n);
        if(phong < 0) phong = 0;
        double speceffect = obj.specularc * pow(phong, obj.shininess);
        col.R += obj.diffusec * obj.color[0] * lambert + speceffect;
        col.G += obj.diffusec * obj.color[1] * lambert + speceffect;
        col.B += obj.diffusec * obj.color[2] * lambert + speceffect;
    }
    //cout << "error check 6" << endl;
    //cout << "error check 7" << endl;
    if(level == 0)
    {
        //cout << "returning value" << endl;
        return col;
    }
    //cout << "error check 8" << endl;
    Ray reflectedRay;
    obj.n.normalize();
    reflectedRay.rd = reflection(r.rd, obj.n);
    reflectedRay.rd.normalize();
    reflectedRay.ro = obj.p + (reflectedRay.rd * 0.001);
    RGB colnext = computePhong(reflectedRay, level - 1);
    //cout << "error check 6" << endl;
    if(colnext.isIntersect == true)
    {
        col.R = (1-obj.reflectionc) * col.R + (obj.reflectionc) * colnext.R;
        col.G = (1-obj.reflectionc) * col.G + (obj.reflectionc) * colnext.G;
        col.B = (1-obj.reflectionc) * col.B + (obj.reflectionc) * colnext.B;
    }
    return col;
}

void saveImage()
{
    //recurLevel = 0;
    bitmap_image image(pixel, pixel);
    populateRayList();
    for(int i = 0; i<pixel; i++)
    {
        for(int j = 0; j<pixel; j++)
        {
            //cout << "phongref is problematic " << i << " " << j << " ";
            //rayList[i][j].print();
            RGB phongref = computePhong(rayList[i][j], recurLevel);
            //cout << "phongref is obtained" << endl;
            if(phongref.R < 0) phongref.R = 0;
            if(phongref.G < 0) phongref.G = 0;
            if(phongref.B < 0) phongref.B = 0;
            //if(phongref.R + phongref.G + phongref.B > 0.003) cout << "phongref = " << (255*phongref.R) << " " << (255*phongref.G) << " " << (255*phongref.B) << endl;
            image.set_pixel(pixel - i - 1, j, 255 * phongref.R, 255 * phongref.G, 255 * phongref.B);
        }
    }
    image.save_image("out.bmp");
    cout << "image saved" << endl;
}



int main(int argc, char **argv){
    ifstream desc;
    desc.open("description.txt");
    string line;
    vector<string> elems;
    int linecount = 1;
    int objectsDeclared = 0;
    int lightsDeclared = 0;
    while(true)
    {
        getline(desc, line);
        if(line.length() == 0)
        {

            //cout << line.length() << " the string is " << line << endl;
            continue;
        }
        if(linecount == 1)
        {
            recurLevel = atoi(line.c_str());
            linecount++;
            continue;
        }
        if(linecount == 2)
        {
            pixel = atoi(line.c_str());
            linecount++;
            continue;
        }
        if(linecount == 3)
        {
            noOfObjects = atoi(line.c_str());
            pyramids = new Pyramid[noOfObjects];
            spheres = new Sphere[noOfObjects];
            linecount++;
            continue;
        }
        if(linecount == 4)
        {
            if(line.compare("sphere") == 0)
            {
                getline(desc, line);
                elems = split(line, ' ');
                Point c;
                c.x = atof(elems[0].c_str());
                c.y = atof(elems[1].c_str());
                c.z = atof(elems[2].c_str());
                spheres[spheresDeclared].centre = c;
                getline(desc, line);
                spheres[spheresDeclared].radius = atof(line.c_str());
                getline(desc, line);
                elems = split(line, ' ');
                spheres[spheresDeclared].color[0] = atof(elems[0].c_str());
                spheres[spheresDeclared].color[1] = atof(elems[1].c_str());
                spheres[spheresDeclared].color[2] = atof(elems[2].c_str());
                getline(desc, line);
                elems = split(line, ' ');
                spheres[spheresDeclared].ambientc = atof(elems[0].c_str());
                spheres[spheresDeclared].diffusec = atof(elems[1].c_str());
                spheres[spheresDeclared].specularc = atof(elems[2].c_str());
                spheres[spheresDeclared].reflectionc = atof(elems[3].c_str());
                getline(desc, line);
                spheres[spheresDeclared].shininess = atof(line.c_str());
                spheresDeclared++; objectsDeclared++;
            }
            if(line.compare("pyramid") == 0)
            {
                getline(desc, line);
                elems = split(line, ' ');
                Point lp;
                lp.x = atof(elems[0].c_str());
                lp.y = atof(elems[1].c_str());
                lp.z = atof(elems[2].c_str());
                pyramids[pyramidsDeclared].lowestP = lp;
                getline(desc, line);
                elems = split(line, ' ');
                pyramids[pyramidsDeclared].width = atof(elems[0].c_str());
                pyramids[pyramidsDeclared].height = atof(elems[1].c_str());
                getline(desc, line);
                elems = split(line, ' ');
                pyramids[pyramidsDeclared].color[0] = atof(elems[0].c_str());
                pyramids[pyramidsDeclared].color[1] = atof(elems[1].c_str());
                pyramids[pyramidsDeclared].color[2] = atof(elems[2].c_str());
                getline(desc, line);
                elems = split(line, ' ');
                pyramids[pyramidsDeclared].ambientc = atof(elems[0].c_str());
                pyramids[pyramidsDeclared].diffusec = atof(elems[1].c_str());
                pyramids[pyramidsDeclared].specularc = atof(elems[2].c_str());
                pyramids[pyramidsDeclared].reflectionc = atof(elems[3].c_str());
                getline(desc, line);
                pyramids[pyramidsDeclared].shininess = atof(line.c_str());
                pyramidsDeclared++; objectsDeclared++;
            }
            if(objectsDeclared == noOfObjects)
            {
                linecount++;
                continue;
            }
        }
        if(linecount == 5)
        {
            noOfLights = atoi(line.c_str());
            //cout << "noOfLights = " << noOfLights << endl;
            lights = new LightSource[noOfLights];
            linecount++;
            continue;
        }
        if(linecount == 6)
        {
            elems = split(line, ' ');
            Point p;
            p.x = atof(elems[0].c_str());
            p.y = atof(elems[1].c_str());
            p.z = atof(elems[2].c_str());
            lights[lightsDeclared].pos = p;
            lightsDeclared++;
            if(lightsDeclared == noOfLights) break;
        }
        //cout << linecount << endl;
    }
//    cout << recurLevel << endl;
//    cout << pixel << endl;
//    cout << noOfObjects << endl;
//    for(int i = 0; i<spheresDeclared; i++)
//    {
//        spheres[i].print();
//    }
//    for(int i = 0; i<pyramidsDeclared; i++)
//    {
//        pyramids[i].print();
//    }
//
//    cout << noOfLights << endl;
//    for(int i = 0; i<lightsDeclared; i++)
//    {
//        cout << lights[i].pos.x << " " << lights[i].pos.y << " " << lights[i].pos.z << endl;
//    }
    rayList = new Ray*[pixel];
    for(int i = 0; i<pixel; i++)
    {
        rayList[i] = new Ray[pixel];
    }
	glutInit(&argc,argv);
	glutInitWindowSize(pixel, pixel);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("Ray Tracing Assignment");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
